import tensorflow as tf
import numpy as np
import scipy
from scipy.io import wavfile

input_file = 'input/beethoven7th2nd-8bit22050hzmono.wav'
#input_file = 'input/beethovenfurelise-8bit22050hzmono.wav'
#input_file = 'input/beethovenfurelise-16bit22050hzmono.wav'
#input_file = "input/440hzsine-8bit22050hzmono.wav"
output_file = 'beethoven/generated-8bit22050hzmono.raw'
#input_file = "input/440hzsine-8bit22050hzmono.wav"
#input_file = "/tmp/inputfile.wav"
#output_file = 'output/generated-8bit22050hzmono.raw'

#For colab:
#external_file = "https://gitlab.com/michael-lewis/wav-nn/-/raw/master/input/440hzsine-8bit22050hzmono.wav?inline=false"
#!wget --no-check-certificate $external_file -O /tmp/inputfile.wav
#input_file = "/tmp/inputfile.wav"
#output_file = '/content/drive/My Drive/Colab Notebooks/generated-8bit22050hzmono.raw'
#checkpoint_dir = '/content/drive/My Drive/Colab Notebooks/checkpoints/'

window_size = 300 # The maximum length we want for a single input
batch_size = 64 # Batch size
shuffle_buffer_size = 10000 # Buffer size to shuffle the dataset

# Configure checkpoints
checkpoint_dir = './beethoven' # Directory where the checkpoints will be saved

epochs = 50

# Number of samples in the output file
output_length = 220500 # 10 seconds at 22.05kHz

def load_wavfile_as_array():
    #with open(config.input_file, 'rb') as wavfile:
    #  wavbytearray = bytearray(wavfile.read())
    #wavarray = np.array(wavbytearray)
    samplerate, wavarray = wavfile.read(input_file)
    # We don't need to change the type if the first layer is not an RNN/LSTM because
    # in those cases it will take values of type uint8.
    # However, if we use an RNN/LSTM in the first layer, we need to use one of
    # bfloat16, float16, float32, float64, int32, int64, complex64, complex128
    wavarraytype = wavarray.dtype
    if wavarraytype == "uint8":
        print("Converting to int32.")
        #print(wavarray[200000:200100])
        wavarray = wavarray.astype(np.int32) # convert to int32
        #info = np.iinfo(wavarray.dtype)
        #wavarray = wavarray.astype(np.float16) / (info.max + 1) # convert to float16 and normalise
        #print(wavarray[200000:200100])
    return samplerate, wavarray

def get_split(wavarray):
    split = int(len(wavarray) * 0.8)
    return split

# Function to window the data

def create_windowed_dataset(input_array, window_size, batch_size, shuffle_buffer):
    # We don't need expand_dims if the first layer is not an RNN/LSTM, because 
    # in those cases we only need a 2 dimensional shape, i.e. batch size and number of time steps
    # However, if we use an RNN/LSTM in the first layer, the input shape has to be 3 dimensions, i.e.
    # batch size, number of time steps and series dimensionality (1 for univariate, appropriate no for multi)
    # An Embedding layer as the first layer would also address this
    input_array = tf.expand_dims(input_array, axis=-1)
    ds = tf.data.Dataset.from_tensor_slices(input_array)
    ds = ds.window(window_size + 1, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda window: window.batch(window_size + 1))
    ds = ds.shuffle(shuffle_buffer)
    ds = ds.map(lambda window: (window[:-1], window[-1])) # Window is all but the last, and the last
    ds = ds.batch(batch_size).prefetch(1)
    return ds

# Define the model

def build_model():
    model = tf.keras.models.Sequential([
        tf.keras.layers.Conv1D(filters=32, kernel_size=5,
            strides=1, padding="causal", activation="relu", input_shape=[None, 1]),
        tf.keras.layers.LSTM(256, return_sequences=True),
        tf.keras.layers.LSTM(64),
        tf.keras.layers.Dense(1)
    ])
    return model

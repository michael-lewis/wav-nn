import tensorflow as tf
import numpy as np
import os
import time
from datetime import datetime
import scipy
from scipy.io import wavfile
import common
import math
#import matplotlib.pyplot as plot

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 

print("Starting at " + str(datetime.now()))

samplerate, wavarray = common.load_wavfile_as_array()

model = common.build_model()
model.reset_states()
model.load_weights(tf.train.latest_checkpoint(common.checkpoint_dir))
#print(model.summary())

# Function to generate new wavarray of output_length using initial_wavarray to seed the predictions

def generate_wav(model, initial_wavarray, output_length):
    generated_wav = []
    progress = common.output_length / 10
    input_wavarray = initial_wavarray#[0:common.window_size-1]
    for i in range(common.output_length):
        if i % progress == 0:
            print(str(i) + " of " + str(common.output_length) + " completed at " + str(datetime.now()))
        # input_wavarray is shape (batch_size, sequence_length/window_size), e.g. shape (1, 300)
        # need it to be (batch_size, sequence_length/window_size, series dimensionality), e.g. shape (1, 300, 1)
        prediction_input = input_wavarray[:][np.newaxis]
        #print("The last 20 bytes from prediction input:")
        #print(prediction_input[0, -20:])
        prediction_input = tf.expand_dims(prediction_input, axis=-1)
        prediction = model.predict(prediction_input)
        # Convert from np.int64 to avoid the following error:
        #tensorflow.python.framework.errors_impl.NotFoundError: Could not find valid device for node.
        #Node:{{node Conv2D}}
        #All kernels registered for op Conv2D :
        #  device='XLA_GPU'; T in [DT_FLOAT, DT_DOUBLE, DT_INT32, DT_BFLOAT16, DT_HALF]
        prediction = tf.cast(prediction, tf.int32)
        [predicted_value] = np.array(prediction)[:, 0]
        #print("The prediction:")
        #print(predicted_value)
        input_wavarray = np.append(input_wavarray[1:], [predicted_value])
        generated_wav.append(predicted_value)
    print("completed at " + str(datetime.now()))
    return generated_wav

# First prediction will be for the first sample after original file end
#initial_input = wavarray[wavarray.size - common.window_size:]
# First prediction will be for the first sample at the end of the first window
initial_input = wavarray[:common.window_size]

#print("The last 20 bytes from the wav file:")
#print(wavarray[-20:])
#print("The last 20 bytes from the initial input:")
#print(initial_input[-20:])

generated_wav = np.array(generate_wav(model, initial_input, common.output_length), dtype=np.uint8)
#print("The generated data")
#print(generated_wav)

#plot.plot(generated_wav)
#plot.show()

wavfile.write(common.output_file, samplerate, generated_wav)

print("Completed at " + str(datetime.now()))

import numpy as np
import math
import matplotlib.pyplot as plot
from scipy.io import wavfile

sample_rate = 22050
duration = 2 # seconds
frequency = 440 # Hz
#wav_format = np.int16
#file_name = "input/100hzsine-16bit22050hzmono.wav"
wav_format = np.uint8
file_name = "input/440hzsine-8bit22050hzmonov2.wav"

if wav_format == np.uint8: # i.e. if unsigned
    amplitude = np.iinfo(wav_format).max / 2 # divide by 2 if unsigned
else:
    amplitude = np.iinfo(wav_format).max

#number_of_samples = sample_rate * duration
#time_of_one_sample = 1 / sample_rate

time_sequence = np.linspace(0, duration, sample_rate * duration)
angular_frequency = 2 * np.pi * frequency
sine_wave = amplitude * np.sin(angular_frequency * time_sequence) # default float64
if wav_format == np.uint8: # i.e. if unsigned
    sine_wave = sine_wave + amplitude # add amplitude if unsigned
sine_wave = sine_wave.astype(wav_format) # convert from float64 to wav_format

plot.plot(time_sequence[:100], sine_wave[:100])
plot.show()

wavfile.write(file_name, sample_rate, sine_wave)

input_sample_rate, wavarray = wavfile.read(file_name)
#wavarray = wavarray.astype(np.uint8)

print(wavarray[:100])

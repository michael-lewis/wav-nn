import tensorflow as tf
import numpy as np
import os
import time
from datetime import datetime
import scipy
from scipy.io import wavfile
import beethoven.common
#import matplotlib.pyplot as plot

#For colab:
#from google.colab import drive
#drive.mount('/content/drive')

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
# 0 = all messages are logged (default behavior)
# 1 = INFO messages are not printed
# 2 = INFO and WARNING messages are not printed

print("Starting at " + str(datetime.now()))

# Load the data

samplerate, wavarray = beethoven.common.load_wavfile_as_array()

# Split into training and validation

split = beethoven.common.get_split(wavarray)
train_array = wavarray[:split]
validate_array = wavarray[split:]
train_dataset = beethoven.common.create_windowed_dataset(train_array, beethoven.common.window_size, beethoven.common.batch_size, beethoven.common.shuffle_buffer_size)
validation_dataset = beethoven.common.create_windowed_dataset(validate_array, beethoven.common.window_size, beethoven.common.batch_size, beethoven.common.shuffle_buffer_size)

# Build the model

optimizer = tf.keras.optimizers.SGD(lr=1e-6, momentum=0.9)
model = beethoven.common.build_model()

# To pick up training from the last saved checkpoint, un-comment-out model.load_weights(). 
# Note that if resuming training in this way, it will start from epoch 1 again rather than 
# the epoch number you left off at, and overwrite the original epoch files, so it might not 
# look like it has resumed training, but looking at the loss should confirm it is using the 
# last saved weights. Manual cleanup of some checkpoints may also be necessary if it stops 
# again before the previous last checkpoint.
#model.load_weights(tf.train.latest_checkpoint(beethoven.common.checkpoint_dir))

model.compile(loss="mse", optimizer=optimizer, metrics=['accuracy'])

# Save model on every epoch
# TODO: Save only 1st and every 5th epoch

#class CustomSaver(tf.keras.callbacks.Callback):
#    def on_epoch_end(self, epoch, logs={}):
#        checkpoint_prefix = os.path.join(common.checkpoint_dir, "ckpt_{epoch}")
#        if epoch == 1 or if (epoch + 1) % 5 == 0:
#            self.model.save(checkpoint_prefix)
#checkpoint_saver = CustomSaver()

checkpoint_prefix = os.path.join(beethoven.common.checkpoint_dir, "ckpt_{epoch}")
checkpoint_callback=tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_prefix,
    save_weights_only=True)

# Now run the model

history = model.fit(train_dataset, validation_data=validation_dataset, epochs=beethoven.common.epochs, callbacks=[checkpoint_callback])

# TODO:  Currently getting the following message at the end of each epoch when running via Jupyter,
# suggesting that the validation array or something is too long 
#	 [[IteratorGetNext/_2]]
#...: W tensorflow/core/common_runtime/base_collective_executor.cc:217] BaseCollectiveExecutor::StartAbort Out of range: End of sequence
#	 [[{{node IteratorGetNext}}]]

loss=history.history['loss']
val_loss=history.history['val_loss']
epochs=range(len(loss))
#plot.plot(epochs, loss, 'r')
#plot.plot(epochs, val_loss, 'b')
#plot.title('Training loss')
#plot.xlabel("Epochs")
#plot.ylabel("Loss")
#plot.legend(['train', 'validation'], loc='upper right')
#plot.figure()

print("Finishing at " + str(datetime.now()))

#Epoch 1/10
#547/547 [==============================] - 50s 91ms/step - loss: 17736.2716 - accuracy: 0.0165 - val_loss: 12635.7758 - val_accuracy: 0.0163

# wav-nn

wav-nn: Generating music wave files with a Neural Network

Details in [wav-nn: Generating music as an audio waveform with a Neural Network](https://www.michael-lewis.com/posts/wav-nn-generating-wav-files-with-a-neural-network/).

## Setting up the Tensorflow dev environment (with Docker and NVIDIA Container Toolkit)

See [Setting up a Tensorflow dev env with Docker & NVIDIA Container Toolkit](https://www.michael-lewis.com/posts/setting-up-tensorflow-with-docker-and-nvidia-container-toolkit/).

## Usage

There's a couple of ways I've been using this:
- From the command line, locally only
- From Jupyter Notebooks, either locally or in colab

### Command line

Create a folder for the checkpoints and configurable data (model, hyperparameters, input_file, output_file, split, etc.) and edit both train.py and generate.py to load the config from that folder, then train on input_file with:

```
python train.py
```

And generate output_file with:

```
python generate.py
```

### Jupyter Notebook

Create a new folder, copy the Notebook to that folder, edit the config, and run interactively as per notes in Notebook.

